Log Microservice
=====

**logs** are one of the most important parts of any system. It helps to detect issues, bugs or crashes. Also, it helps to explain the system performance and the expected changes.

#### THE CHALLENGE
Create log microservice which should store, list, and search for logs.

> The log object should contain a log title, description, status code,log path and created at date.

#### REQUIREMENTS
* Design the required schema and use MongoDB for the database.
* Design the required API endpoints for the system.
* Secure you endpoints using JWT authentication.
* Assume that the system is to receive many requests. It might be running on multiple servers in
parallel and thus multiple requests may be processed concurrently. Make sure to handle race
conditions. Try to minimize the queries and avoid writing directly to database while serving the
requests(especially for the log creation endpoints). You can use a queuing
system to achieve that. It is allowed for chats and messages to take time to be persisted. You
should optimize your tables by adding appropriate indices.
* Assume that you're dealing with multiple agents with different credentials.
* After creating log, send an email notification to system adminstrators (list of emails).
* Use swagger API documentation for your API design.
* Test your code using unit testing.

#### RESTRICTIONS
* Use any NodeJS framework (preferably hapijs).
* Setup a gitlab repository on your personal gitlab account that will contain the
  whole project.
* Service should be run by docker-compose up.
* **[Bonus]** Add GitLab compatible CI/CD to perform a server deploy.
  
#### CRITERIA
> The code will be judged on the following criteria:
1. Strategy for building the system structure.
4. Readability and inline documentation (preferably code should explain itself and
not need additional comments).
7. README file to setup working solution.
8. The system should be functional and complete.